# cloudflare-workers-template

Automatic deployment of CloudFlare Workers by GitLab CI.

## Setup

1. Set the following environment variables in project Settings -> CI/CD, which will be automatically fill in to the deployment.

````
WORKERS_NAME
CF_ACCOUNT_ID
CF_ZONE_ID
ROUTE
````

2. Then put your source code under `./src`. Modify `package.json` to point to the main js correspondingly. If the source code resides on another repo, it is recommended to utilize Git submodule to mirror the third-party source code.

3. `git push` and the CI will make it automatically.